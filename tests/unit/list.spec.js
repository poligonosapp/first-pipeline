import { expect } from 'chai';
import { mount } from '@vue/test-utils';
import List from '@/components/List.vue';
import Vue from 'vue';

describe('List.vue', () => {
  // previous tests ..
  it('displays items from the list', () => {
    const ListComponent = mount(List);
    expect(ListComponent.text()).to.contain('play games');
  });

  it('adds new item to list on click', () => {
    // build component
    const ListComponent = mount(List);

    // set input value
    ListComponent
      .find('#input')
      .setValue('brush my teeth');

    // simulate click event
    const button = ListComponent.find('#button');
    button.trigger('click');

    // assert list contains new item
    Vue.nextTick(() => {
      expect(ListComponent.text()).to.contain('brush my teeth');
    });
  });
});
